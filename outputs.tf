output "ecr_urls" {
  value = { for key, repo in aws_ecr_repository.repos : key => repo.repository_url }
}
